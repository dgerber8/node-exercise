const swapiPeople = require('../integration/getPeople');
const sortPeople = require('./tasks/sortPeople');

// This function communicates with the integration layer to retrieve an array of Person objects.
// It then uses another function in the services layer to sort the objects by a specified 'sortBy'
// value from the URL's query string if there is one. It then returns that array

async function getPeopleArray(reqQuery) {
    // Stores the people array retrieved from the integration layer's HTTP request to SWAPI
    let peopleArray = await swapiPeople.getPeople();
    // If sortBy has been defined, sorts the people array before returning it
    if (reqQuery.sortBy) {
        peopleArray = sortPeople.sortByQuery(reqQuery.sortBy, peopleArray);
    }
    
    return peopleArray;
}

module.exports.getPeopleArray = getPeopleArray;