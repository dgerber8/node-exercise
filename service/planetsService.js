const swapiPlanets = require('../integration/getPlanets');
const swapiResidentNames = require('../integration/getResidentNames');

// This function communicates with the integration layer to retrieve an array of Planet objects and returns that array
async function getPlanetsArray() {
    // Stores the people array retrieved from the integration layer's HTTP request to SWAPI
    let planetsArray = await swapiPlanets.getPlanets();

    return planetsArray;
}

module.exports.getPlanetsArray = getPlanetsArray;