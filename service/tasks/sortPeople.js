// This function takes in an array of people objects as well as a criteria to sort them by (name, mass, or height)
// sorts in ascending order, then returns the sorted array
function sortByQuery(sortBy, peopleArray) {
    // Sorts people alphabetically by name field in ascending order
    if (sortBy === 'name') {
        peopleArray.sort(function(a,b) {
            if (a.person.name.toLowerCase() < b.person.name.toLowerCase()) {
                return -1;
            }
            if (a.person.name.toLowerCase() > b.person.name.toLowerCase()){
                return 1;
            }
            return 0;
        });
        return peopleArray;
    }
    // Sorts people numerically by height field in ascending order. Comma seperated numbers are properly handled and 'unknown' values are pushed to the end of the list
    if (sortBy === 'height') {
        peopleArray.sort(function(a,b) {
            // Evaluate values as equal if both are 'unknown'
            if (b.person.height === 'unknown' && a.person.height ==='unknown') return 0;
            // If only one value is 'unknown', the unknown value is interpreted as the greater value (aka it gets sorted later in the list) 
            else if (b.person.height === 'unknown') return -1;
            else if( a.person.height === 'unknown') return 1;
            // If neither are unknown, actually compares the numerical values (includes proper handling for comma seperated numbers)
            else return parseFloat (a.person.height.replace(/,/g, '')) - parseFloat(b.person.height.replace(/,/g, ''));
            //return parseFloat(a.person.height) - parseFloat(b.person.height);
        });
        return peopleArray;
    }
    // Sorts people numerically by mass field in ascending order. Comma seperated numbers are properly handled and 'unknown' values are pushed to the end of the list
    if (sortBy === 'mass') {
        peopleArray.sort(function(a,b) {
            // Evaluate values as equal if both are 'unknown'
            if (b.person.mass === 'unknown' && a.person.mass ==='unknown') return 0;
            // If only one value is 'unknown', the unknown value is interpreted as the greater value (aka it gets sorted later in the list) 
            else if (b.person.mass === 'unknown') return -1;
            else if (a.person.mass === 'unknown') return 1;
            // If neither are unknown, actually compares the numerical values (includes proper handling for comma seperated numbers)
            else return parseFloat(a.person.mass.replace(/,/g, '')) - parseFloat(b.person.mass.replace(/,/g, ''));
        });
        return peopleArray;
    }

}

module.exports.sortByQuery = sortByQuery;