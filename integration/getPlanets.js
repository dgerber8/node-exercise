const axios = require("axios");
const swapiResidentNames = require('./getResidentNames');

// This function uses axios to make an HTTP request to SWAPI and retrieve a full paginated list of planet data. The data is translated
// into an array of planet objects. While doing this, the URLs in each planet's 'residents' field are replaced with the content of the 
// 'name' field the corresponding character data that URL is pointing to. The final array of objects is then returned
async function getPlanets() {
    // Defines a framework for a planet object and loads all planet HTTP response data into that framework
    async function presentPlanetItem(item) {
        // Before storing resident urls in the object, replaces them with their corresponding full name strings
        const residentNames = await swapiResidentNames.getResidentNames(item.residents);
        return {
            planet: {
                name: item.name,
                rotation_period: item.rotation_period,
                orbital_period: item.orbital_period,
                diameter: item.diameter,
                climate: item.climate,
                gravity: item.gravity,
                terrain: item.terrain,
                surface_water: item.surface_water,
                population: item.population,
                residents: residentNames,
                films: item.films,
                created: item.created,
                edited: item.edited,
                url: item.url,
            },
        };
    }

    // This section maps the resulting JSON planet data into an array of planet objects. That array is returned
    let response;
    let allPlanets = [];
    let nextURL = 'https://swapi.dev/api/planets';

    // Loops through each page by following each response's 'next' values until a 'next' value is 
    // null which would mean there are no more pages to load
    do {
        // Uses Axios to execute an HTTP request to SWAPI and stores the HTTP response. Then moves on to the next page if there is one
        response = await axios.get(nextURL);
        let toAdd = await Promise.all(response.data.results.map(presentPlanetItem));
        allPlanets.push(...toAdd);
        nextURL = response.data.next
    } while (response.data.next)

    // Checks if all planet objects were successfully loaded. If so, the length of our planet array should equal the value in the 'count' field
    if (allPlanets.length !== response.data.count ) {
        throw 'Planet pagination failed. Array length does not equal count value';
    }
    
    return allPlanets;
}

module.exports.getPlanets = getPlanets;