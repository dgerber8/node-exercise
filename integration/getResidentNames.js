const axios = require("axios");

// This function takes an array containing URLs to individual residents on a planet and returns
// an array where each url is replaced with the first name of the corresponding resident.
async function getResidentNames(residentUrls) {
    let names = [];
    // Loops through each URL, makes an HTTP request to it with axios, then returns the contents of the HTTP reponse's name field
    await Promise.all(residentUrls.map(async (nameLink) => {
        if(nameLink) {
            const response = await axios.get(nameLink);
            const name = response.data.name;
            names.push(name);
        }
    }));
    return names;
}

module.exports.getResidentNames = getResidentNames;