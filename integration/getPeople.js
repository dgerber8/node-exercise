const axios = require("axios");

// This function uses axios to make an HTTP request to SWAPI and retrieve a full paginated list of people data. The data is translated
// into an array of people objects which is then returned.
async function getPeople() {
    // Defines a framework for a person object
    function presentPersonItem(item) {
        return {
            person: {
                name: item.name,
                height: item.height,
                mass: item.mass,
                hair_color: item.hair_color,
                skin_color: item.skin_color,
                eye_color: item.eye_color,
                birth_year: item.birth_year,
                gender: item.gender,
                homeworld: item.homeworld,
                films: item.films,
                species: item.species,
                vehicles: item.vehicles,
                starships: item.starships,
                created: item.created,
                edited: item.edited,
                url: item.url,
            },
        };
    }

    // This section maps the resulting JSON person data into an array of person objects. That array is returned.    
    let response;
    let allPeople = [];
    let nextURL = 'https://swapi.dev/api/people';

    // Loops through each page until 'next' value is null which would mean there are no more pages to load
    do {
        // Uses Axios to execute an HTTP request to SWAPI and stores the HTTP response
        response = await axios.get(nextURL);
        allPeople.push(...response.data.results.map(presentPersonItem));
        nextURL = response.data.next
    } while (response.data.next)

    // Check if all people objects were successfully loaded. The length of our people array should equal the value in the 'count' field
    if (allPeople.length !== response.data.count ) {
        throw 'People pagination failed. Array length does not equal count value';
    }

    return allPeople;
}

module.exports.getPeople = getPeople;