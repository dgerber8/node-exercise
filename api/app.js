const express = require('express');
const routes = require("./routes/index");
// Initializes instance of express as 'app'
const app = express();
app.use(express.json());

// Defines usage of all URL paths based on behaviour defined in /routes/index.js
app.use("/", routes);

module.exports = app;