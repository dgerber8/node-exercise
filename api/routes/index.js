const express = require('express');
const asyncHandler = require("express-async-handler");

// Dependency on service layer
const peopleService = require('../../service/peopleService');
const planetsService = require('../../service/planetsService');
const router = new express();

// Defines behaviour for a GET request to '/people'
router.get('/people', asyncHandler(async (req, res, next) => {
        console.log('Retrieving people...');
        // Error handling for invalid inputs
        if (Object.keys(req.query).length !== 0) {
            if (!req.query.sortBy) {
                // Error handling for if a query string parameter exist and it is not 'sortBy'
                res.status(400).send('Error: Invalid query string parameter(s)');
                return;
            }
            // Error handling for if the set 'sortBy' parameter isn't 'mass', 'height', or 'name'
            if (req.query.sortBy !== 'mass' && req.query.sortBy !== 'height' && req.query.sortBy !== 'name') {
                res.status(400).send('Error: Unable to sort due to invalid \'sortBy\' value');
                return;
            }
        }

        // Calls on the service layer with the request's optional query string parameter ('sortBy') and stores a pre-processed people array
        const people = await peopleService.getPeopleArray(req.query);
        // Responds to the user witht the processed people array
        res.send(people);
})); 

// Defines behaviour for a GET request to '/planets'
router.get('/planets', asyncHandler(async (req, res, next) => {
    console.log('Retrieving planets...');
    // Calls on the service layer and stores a pre-processed planets array
    const planets = await planetsService.getPlanetsArray();
    // Responds to the user witht the processed planets array
    res.send(planets);
}));

// Listens on port defined in PORT env variable. If undefined, listens on port 3000
const port = process.env.PORT || 3000;
router.listen(port, () => console.log(`Listening on port ${port}...`));

module.exports = router;